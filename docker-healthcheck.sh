#!/bin/bash

# debug
if [ -n "${DEBUG}" ]; then
  set -eux
else
  set -e
fi

if ps aux | pgrep nginx; then
  if [[ "$(curl -s http://localhost/)" ]]; then
    echo "success"
    exit 0
  fi
fi
echo "unhealthy"
exit 1
