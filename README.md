# Docker LDAP Account Manager

LDAP Account Manager Alpine Linux with HTTP Auth entry point.

##### Image tag info

Please note that the image with the tag "latest" uses the source code of the master branch.
We strongly recommend using only images with stability tags.

## Extensions

  * Health check

## Config

#### Run Example

`docker run --name ldapadmin -p 8080:80 codevit/ldapadmin:stable`

#### Env Vars

  * LAM_URI             => Uri to ldap server
  * LAM_BASE            => LDAP base dn (*required)
  * LAM_ADMIN_BASE      => LDAP admin base dn (*required)
  * LAM_ADMIN_PASSWD    => LDAP admin password (*required)

#### Test Example

  * https://bitbucket.org/codev-it/docker-ldap-account-manager/src/master/test/