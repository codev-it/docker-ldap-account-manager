#!/bin/bash

# debug
if [ -n "${DEBUG}" ]; then
  set -eux
else
  set -e
fi

# helpers
_gotpl() {
  if [[ -f "/etc/gotpl/$1" ]]; then
    gotpl "/etc/gotpl/$1" >"$2"
  fi
}

# build nginx config
NGINX_ROOT="/var/www/html"
NGINX_USER="nginx"

# nginx settings
_gotpl "nginx/vhost.conf.tmpl" "/etc/nginx/conf.d/vhost.conf"
_gotpl "nginx/app.conf.tmpl" "/etc/nginx/app.conf"

# check lam uri env. var
if [[ -z "${LAM_URI}" ]]; then
  echo >&2 'Error:  You need to specify LAM_URI'
  exit 1
fi

# check lam base env. var
if [[ -z "${LAM_BASE}" ]]; then
  echo >&2 'Error:  You need to specify LAM_BASE'
  exit 1
fi

# check lam admin base env. var
if [[ -z "${LAM_ADMIN_PASSWD}" && -z "${LAM_ADMIN_PASSWD_FILE}" ]]; then
  echo >&2 'Error:  You need to specify LAM_ADMIN_PASSWD or LAM_ADMIN_PASSWD_FILE'
  exit 1
fi

# update ldap admin base
if [ -z "${LAM_ADMIN_BASE}" ]; then
  export LAM_ADMIN_BASE="cn=admin,${LAM_BASE}"
fi

# create ssha password for lam
if [[ -n "${LAM_ADMIN_PASSWD_FILE}" && -f "${LAM_ADMIN_PASSWD_FILE}" ]]; then
  lam_admin_passwd_file_content="$(cat ${LAM_ADMIN_PASSWD_FILE})"
  export LAM_ADMIN_PASSWD_SSHA="$(lam-generate-ssha ${lam_admin_passwd_file_content})"
else
  export LAM_ADMIN_PASSWD_SSHA="$(lam-generate-ssha ${LAM_ADMIN_PASSWD})"
fi

## build lam configs
if [ -d "${NGINX_ROOT}/config" ]; then
  mkdir -p "${NGINX_ROOT}/config"
fi

CONFIG_FILE="${NGINX_ROOT}/config/config.cfg"
if [ ! -f "${CONFIG_FILE}" ]; then
  _gotpl "lam/config.cfg.tmpl" "${CONFIG_FILE}"
  chown -R ${NGINX_USER}:${NGINX_USER} "${CONFIG_FILE}"
fi

LAM_FILE="${NGINX_ROOT}/config/default.conf"
if [ ! -f "${LAM_FILE}" ]; then
  _gotpl "lam/default.conf.tmpl" "${LAM_FILE}"
  chown -R ${NGINX_USER}:${NGINX_USER} "${LAM_FILE}"
fi

if [[ -z "$@" ]]; then
  # Start supervisord and services
  chown -R ${NGINX_USER}:${NGINX_USER} "${NGINX_ROOT}"
  exec /usr/bin/supervisord --nodaemon -c /etc/supervisord.conf
else
  exec "$@"
fi
