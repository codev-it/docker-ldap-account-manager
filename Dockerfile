# Builder
FROM php:7.2-fpm-alpine as builder
MAINTAINER Codev-IT <office@codev-it.at>

# defaults env.
ARG LAM_VERSION=7.2
ENV LAM_PACK="ldap-account-manager-${LAM_VERSION}" \
    GOTPL_VER=0.1.5 \
    GOROOT=/usr/lib/go \
    GOPATH=/var/www/auth

# build proxy-sql from source
WORKDIR /tmp

# install depends
RUN set -xe \
  && mkdir -p /src \
  && apk add --update \
    go \
    git \
    wget \
    perl \
    bash \
    build-base \
    util-linux

# whereis copy
COPY bin/whereis_copy /usr/bin/whereis_copy
RUN set -xe \
  && chmod +x /usr/bin/whereis_copy

# prepare php
RUN set -xe \
  && whereis_copy php /src \
  && whereis_copy php-fpm /src \
  && mkdir -p /src/usr/src \
  && cp -R /usr/local/bin /src/usr/local \
  && mkdir -p /src/usr/local/include \
  && cp -R /usr/local/include/php /src/usr/local/include \
  && cp -R /usr/src/php.tar.xz /src/usr/src/php.tar.xz \
  && cp -R /usr/src/php.tar.xz.asc /src/usr/src/php.tar.xz.asc

# get LDAP account manager
RUN set -xe \
  && wget "https://downloads.sourceforge.net/project/lam/LAM/${LAM_VERSION}/${LAM_PACK}.tar.bz2" \
  && tar -C /tmp -xjf /tmp/${LAM_PACK}.tar.bz2 \
  && cd ${LAM_PACK} \
  && rm -rf /var/www/html/* \
  && mkdir -p /src/usr/local/lam \
  && ./configure \
    --with-httpd-user=root \
    --with-httpd-group=root \
    --with-web-root=/src/var/www/html \
  && make install \
  && cp -R /usr/local/lam /src/usr/local

# copy local source
COPY src /src
COPY tpl /src/etc/gotpl

# copy docker scripts
COPY docker-entrypoint.sh /src/entrypoint
COPY docker-healthcheck.sh /src/healthcheck

# copy custom bin scripts
COPY bin/lam-generate-ssha /src/usr/bin/lam-generate-ssha

# prepare script permissions
RUN set -eux \
  && chmod +x /src/entrypoint \
  && chmod +x /src/healthcheck \
  && chmod -R +x /src/usr/bin/lam-generate-ssha

# Final
FROM wodby/nginx:1.19
MAINTAINER Codev-IT <office@codev-it.at>

# defualts
ENV LAM_URI="ldap://localhost:389" \
    LAM_BASE="dc=example,dc=com" \
    LAM_ADMIN_BASE="cn=admin,dc=example,dc=com" \
    LAM_ADMIN_PASSWD="lam" \
    LAM_ADMIN_PASSWD_FILE=""

# wodby nginx settings
ENV NGINX_STATIC_CONTENT_OPEN_FILE_CACHE="off" \
    NGINX_BACKEND_HOST="127.0.0.1" \
    NGINX_VHOST_PRESET="php" \
    NGINX_INDEX_FILE="index.html" \
    NGINX_FASTCGI_INDEX="index.html" \
    NGINX_SERVER_ROOT="/var/www/html" \
    ALPINE_MIRROR="http://dl-cdn.alpinelinux.org/alpine"

# php extensions
ENV PHP_INI_DIR=/usr/local/etc/php
ENV PHP_CFLAGS="-fstack-protector-strong -fpic -fpie -O2"
ENV PHP_CPPFLAGS="$PHP_CFLAGS"
ENV PHP_LDFLAGS="-Wl,-O1 -Wl,--hash-style=both -pie"
ENV PHP_EXT iconv \
            gd \
            intl \
            pcntl \
            zip \
            bcmath \
            opcache \
            simplexml \
            xmlrpc \
            soap \
            session \
            readline \
            gettext \
            ldap \
            mbstring \
            gmp

# set user
USER root

# build proxy-sql from source
WORKDIR /var/www/html

# copy build files
COPY --from=builder /src /

# build nginx php
RUN set -eux \
  # defaults
  && apk add --no-cache --update \
    xz \
    curl \
    bash \
    supervisor \
  # install php
  && apk add --update --no-cache -t .build-deps \
    git \
    gcc \
    g++ \
    tar \
    curl \
    re2c \
    wget \
    perl \
    make \
    file \
    gmp-dev \
    pkgconf \
    icu-dev \
    curl-dev \
    autoconf \
    libc-dev \
    coreutils \
    libzip-dev \
    build-base \
    argon2-dev \
    util-linux \
    libpng-dev \
    gettext-dev \
    libedit-dev \
    libxml2-dev \
    openldap-dev \
    freetype-dev \
    dpkg-dev dpkg \
    libsodium-dev \
    ca-certificates \
    libjpeg-turbo-dev \
  && apk add --no-cache --repository \
    http://dl-cdn.alpinelinux.org/alpine/edge/main openssl \
  && docker-php-ext-configure gd \
    --with-gd \
    --with-freetype-dir=/usr/include/ \
    --with-png-dir=/usr/include/ \
    --with-jpeg-dir=/usr/include/ \
  && docker-php-ext-configure bcmath --enable-bcmath \
  && docker-php-ext-configure pcntl --enable-pcntl \
  && docker-php-ext-configure mbstring --enable-mbstring \
  && docker-php-ext-configure soap --enable-soap \
  && docker-php-ext-configure gettext \
  && docker-php-ext-install -j$(nproc) ${PHP_EXT} \
  && docker-php-ext-enable ${PHP_EXT} \
  && ln -s /usr/local/bin/php /usr/bin/php \
  && runDeps="$( \
  		scanelf --needed --nobanner --format '%n#p' --recursive /usr/local \
  			| tr ',' '\n' \
  			| sort -u \
  			| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
  	)" \
  && apk add --no-cache --update $runDeps \
  # ldap config
  && mkdir -p /var/www/html/tmp \
  && mkdir -p /var/www/html/sess \
  && chown nginx:nginx /var/www/html/sess \
  && chown nginx:nginx /var/www/html/tmp \
  # cleanup
  && rm -rf /tmp/* \
  && apk del .build-deps \
  && rm -rf /var/cache/apk/*

# declare volumes
VOLUME /var/www/html

# docker entrypoint and commands
HEALTHCHECK CMD /healthcheck
CMD ["bash", "/entrypoint"]

# ports
EXPOSE 80

# Build-time metadata as defined at http://label-schema.org
ARG BUILD_DATE
ARG VCS_REF
ARG VERSION
LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.name="ldapadmin" \
      org.label-schema.description="LDAP Account Manager based on Nginx and Alpine Linux." \
      org.label-schema.url="https://www.codev-it.at" \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url="https://acoser@bitbucket.org/codev-it/docker-ldap-account-manager.git" \
      org.label-schema.vendor="Codev-IT" \
      org.label-schema.version=$VERSION \
      org.label-schema.schema-version="1.0"